package pl.zoo.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import pl.zoo.admin.model.UserDTO;

import java.util.List;

@SpringBootApplication
public class Main {

    public static void main(String[] args) {

        SpringApplication.run(Main.class, args);

//        RestTemplate restTemplate = new RestTemplate();
//
//        ResponseEntity<UserDTO[]> usersDTO = restTemplate.exchange("http://localhost:8090/users", HttpMethod.GET, HttpEntity.EMPTY, UserDTO[].class);
//        String resultAnimal = restTemplate.getForObject("http://localhost:8091/animals", String.class);
//        String resultPlace = restTemplate.getForObject("http://localhost:8092/places", String.class);
//        String resultIssue = restTemplate.getForObject("http://localhost:8093/issues", String.class);
//        String resultAssign = restTemplate.getForObject("http://localhost:8094/assigns", String.class);
//
//        for (int i = 0; i < usersDTO.getBody().length; i++) {
//
//            System.out.println(usersDTO.getBody()[i]);
//            System.out.println(usersDTO.getBody()[i].getName());
//            System.out.println(usersDTO.getBody()[i].getLastname());
//            System.out.println(usersDTO.getBody()[i].getProfession());
//        }

    }
}


