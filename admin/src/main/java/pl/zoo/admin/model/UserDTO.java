package pl.zoo.admin.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
public class UserDTO {

    @JsonProperty
    private String login;
    @JsonProperty
    private String name;
    @JsonProperty
    private String lastname;
    @JsonProperty
    private String profession;
    @JsonProperty
    private String password;



    public String getLogin() {
        return login;
    }

    public String getName() {
        return name;
    }

    public String getLastname() {
        return lastname;
    }

    public String getProfession() {
        return profession;
    }

    public String getPassword() {
        return password;
    }
}
