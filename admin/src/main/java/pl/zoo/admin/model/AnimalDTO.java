package pl.zoo.admin.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Builder
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
public class AnimalDTO {

    @JsonProperty
    private String species;
    @JsonProperty
    private String food;
    @JsonProperty
    private int numberOfAnimals;
    @JsonProperty
    @Enumerated(EnumType.STRING)
    private Danger danger;

    public String getSpecies() {
        return species;
    }

    public String getFood() {
        return food;
    }

    public int getNumberOfAnimals() {
        return numberOfAnimals;
    }

    public Danger danger() {
        return danger;
    }

}
