package pl.zoo.admin.model;

public enum Danger {

    LOW,
    MEDIUM,
    HIGH,
    VERY_HIGH;
}
