package pl.zoo.admin.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@AllArgsConstructor(access = AccessLevel.PACKAGE)
public class PlaceDTO {

    @JsonProperty
    private String name;
    @JsonProperty
    private double area;

    public String getName(){
        return name;
    }

    public double getArea(){
        return area;
    }
}
