package pl.zoo.admin.api;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.zoo.admin.domain.AdminService;

@RestController
public class LoginApi {

    private AdminService adminService;

    public LoginApi(AdminService adminService) {
        this.adminService = adminService;
    }

    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public ResponseEntity<Void> loginAsAdmin(@RequestParam String login, @RequestParam String password) {
        boolean success = adminService.loginAsAdmin(login, password);
        if (success) {
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN.value()).build();
    }
}
