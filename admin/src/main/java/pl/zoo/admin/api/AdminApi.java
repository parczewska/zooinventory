package pl.zoo.admin.api;

import org.apache.catalina.User;
import org.springframework.web.bind.annotation.*;
import pl.zoo.admin.domain.*;
import pl.zoo.admin.model.AnimalDTO;
import pl.zoo.admin.model.IssueDTO;
import pl.zoo.admin.model.PlaceDTO;
import pl.zoo.admin.model.UserDTO;

import java.util.List;

@RestController
public class AdminApi {

    private UserService userService;
    private AnimalService animalService;
    private PlaceService placeService;
    private IssueService issueService;

    public AdminApi(UserService userService, AnimalService animalService, PlaceService placeService, IssueService issueService) {
        this.userService = userService;
        this.animalService = animalService;
        this.placeService = placeService;
        this.issueService = issueService;
    }

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public List<Object> users() {
        return userService.getAllUsers();
    }

    @RequestMapping(value = "/users/{lastname}", method = RequestMethod.GET)
    public List<Object> getUser(@PathVariable String lastname) {
        return userService.getUser(lastname);
    }

    @RequestMapping(value = "/users", method = RequestMethod.POST)
    public UserDTO addUser(@RequestBody UserDTO userDTO) {
        return userService.addUser(userDTO);
    }

    @RequestMapping(value = "/users/{lastname}", method = RequestMethod.DELETE)
    public void deleteUser(@PathVariable String lastname) {
        userService.deleteUser(lastname);
    }

    @RequestMapping(value = "/animals", method = RequestMethod.GET)
    public List<Object> animals() {
        return animalService.getAllAnimals();
    }

    @RequestMapping(value = "/animals/{species}", method = RequestMethod.GET)
    public List<Object> getAnimal(@PathVariable String species) {
        return animalService.getAnimal(species);
    }

    @RequestMapping(value = "/animals", method = RequestMethod.POST)
    public AnimalDTO addAnimal(@RequestBody AnimalDTO animalDTO) {
        return animalService.addAnimal(animalDTO);
    }

    @RequestMapping(value = "/animals/{species}", method = RequestMethod.DELETE)
    public void deleteAnimal(@PathVariable String species) {
        animalService.deleteAnimal(species);
    }

    @RequestMapping(value = "/places", method = RequestMethod.GET)
    public List<Object> places() {
        return placeService.getAllPlaces();
    }

    @RequestMapping(value = "/places/{name}", method = RequestMethod.GET)
    public List<Object> getPlace(@PathVariable String name) {
        return placeService.getPlace(name);
    }

    @RequestMapping(value = "/places", method = RequestMethod.POST)
    public PlaceDTO addPlace(@RequestBody PlaceDTO placeDTO) {
        return placeService.addPlace(placeDTO);
    }

    @RequestMapping(value = "/places/{name}", method = RequestMethod.DELETE)
    public void deletePlace(@PathVariable String name) {
        placeService.deletePlace(name);
    }

    @RequestMapping(value = "/issues", method = RequestMethod.GET)
    public List<Object> issues() {
        return issueService.getAllIssues();
    }

    @RequestMapping(value = "/issues/{id}", method = RequestMethod.GET)
    public List<Object> getIssue(@PathVariable long id) {
        return issueService.getIssue(id);
    }

    @RequestMapping(value = "/issues", method = RequestMethod.POST)
    public IssueDTO addIssue(IssueDTO issueDTO) {
        return issueService.addIssue(issueDTO);
    }

    @RequestMapping(value = "/issues/{id}", method = RequestMethod.DELETE)
    public void deleteIssue(@PathVariable long id) {
        issueService.deleteIssue(id);
    }
}
