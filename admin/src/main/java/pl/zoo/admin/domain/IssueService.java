package pl.zoo.admin.domain;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import pl.zoo.admin.model.IssueDTO;
import pl.zoo.admin.utils.ObjectMapper;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Service
public class IssueService {

    public List<Object> getAllIssues() {

        ResponseEntity<IssueDTO[]> response = new RestTemplate().exchange("http://localhost:8093/issues", HttpMethod.GET, HttpEntity.EMPTY, IssueDTO[].class);
        if (response.hasBody()) {
            IssueDTO[] arrayIssues = response.getBody();

            return ObjectMapper.arrayToList(arrayIssues);
        }
        return Collections.emptyList();
    }

    public List<Object> getIssue(long id) {
        ResponseEntity<IssueDTO[]> response = new RestTemplate().exchange("http://localhost:8093/issues/" + id, HttpMethod.GET, HttpEntity.EMPTY, IssueDTO[].class);
        if (response.hasBody()) {
            IssueDTO[] arrayIssue = response.getBody();

            return ObjectMapper.arrayToList(arrayIssue);
        }
        return Collections.emptyList();
    }

    public IssueDTO addIssue(IssueDTO issueDTO) {

        HttpEntity httpEntity = new HttpEntity<>(issueDTO);

        ResponseEntity<IssueDTO> response = new RestTemplate().exchange("http://localhost:8093/issue", HttpMethod.POST, httpEntity, IssueDTO.class);

        if (response.hasBody()) {
            return response.getBody();
        }
        return IssueDTO.builder().build();
    }

    public void deleteIssue(long id) {

        new RestTemplate().exchange("http://localhost:8093/issues" + id, HttpMethod.DELETE, HttpEntity.EMPTY, IssueDTO.class);

    }
}
