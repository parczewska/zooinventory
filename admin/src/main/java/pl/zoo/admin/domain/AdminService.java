package pl.zoo.admin.domain;

import org.apache.catalina.User;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import pl.zoo.admin.model.AnimalDTO;
import pl.zoo.admin.model.PlaceDTO;
import pl.zoo.admin.model.UserDTO;
import pl.zoo.admin.utils.ObjectMapper;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Service
public class AdminService {

    public boolean loginAsAdmin(String login, String password) {

        ResponseEntity<UserDTO> response = new RestTemplate().exchange("http://localhost:8090/users/login?login=" + login + "&password=" + password,
                HttpMethod.GET, HttpEntity.EMPTY, UserDTO.class);

        return response.getStatusCode().is2xxSuccessful();
    }

}


