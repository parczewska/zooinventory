package pl.zoo.admin.domain;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import pl.zoo.admin.model.AnimalDTO;
import pl.zoo.admin.utils.ObjectMapper;

import java.util.Collections;
import java.util.List;

@Service
public class AnimalService {

    public List<Object> getAllAnimals() {

        ResponseEntity<AnimalDTO[]> response = new RestTemplate().exchange("http://localhost:8091/animals", HttpMethod.GET, HttpEntity.EMPTY, AnimalDTO[].class);
        if (response.hasBody()) {
            AnimalDTO[] arrayAnimals = response.getBody();

            return ObjectMapper.arrayToList(arrayAnimals);
        }
        return Collections.emptyList();

    }

    public List<Object> getAnimal(String species) {

        ResponseEntity<AnimalDTO[]> response = new RestTemplate().exchange("http://localhost:8091/animals/" + species, HttpMethod.GET, HttpEntity.EMPTY, AnimalDTO[].class);
        if (response.hasBody()) {
            AnimalDTO[] arrayAnimal = response.getBody();

            return ObjectMapper.arrayToList(arrayAnimal);
        }
        return Collections.emptyList();
    }

    public AnimalDTO addAnimal(AnimalDTO animalDTO) {

        HttpEntity httpEntity = new HttpEntity<>(animalDTO);

        ResponseEntity<AnimalDTO> response = new RestTemplate().exchange("http://localhost:8091/animals", HttpMethod.POST, httpEntity, AnimalDTO.class);

        if (response.hasBody()) {
            return response.getBody();
        }
        return AnimalDTO.builder().build();
    }

    public void deleteAnimal(String species) {

        new RestTemplate().exchange("http://localhost:8091/animals/" + species, HttpMethod.DELETE, HttpEntity.EMPTY, AnimalDTO.class);
    }
}
