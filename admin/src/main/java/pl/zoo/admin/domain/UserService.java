package pl.zoo.admin.domain;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import pl.zoo.admin.model.UserDTO;
import pl.zoo.admin.utils.ObjectMapper;

import java.util.Collections;
import java.util.List;

@Service
public class UserService {

    public List<Object> getAllUsers() {

        ResponseEntity<UserDTO[]> response = new RestTemplate().exchange("http://localhost:8090/users", HttpMethod.GET, HttpEntity.EMPTY, UserDTO[].class);
        if (response.hasBody()) {
            UserDTO[] arrayUsers = response.getBody();

            return ObjectMapper.arrayToList(arrayUsers);
        }
        return Collections.emptyList();
    }

    public List<Object> getUser(String lastname) {

        ResponseEntity<UserDTO[]> response = new RestTemplate().exchange("http://localhost:8090/users/" + lastname, HttpMethod.GET, HttpEntity.EMPTY, UserDTO[].class);
        if (response.hasBody()) {
            UserDTO[] arrayUser = response.getBody();

            return ObjectMapper.arrayToList(arrayUser);
        }
        return Collections.emptyList();
    }

    public UserDTO addUser(UserDTO userDTO) {

        HttpEntity httpEntity = new HttpEntity<>(userDTO);

        ResponseEntity<UserDTO> response = new RestTemplate().exchange("http://localhost:8090/users", HttpMethod.POST, httpEntity, UserDTO.class);

        if (response.hasBody()) {
            return response.getBody();
        }
        return UserDTO.builder().build();
    }

    public void deleteUser(String lastname) {

        new RestTemplate().exchange("http://localhost:8090/users/" + lastname, HttpMethod.DELETE, HttpEntity.EMPTY, UserDTO.class);
    }
}
