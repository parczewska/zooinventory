package pl.zoo.admin.domain;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import pl.zoo.admin.model.PlaceDTO;
import pl.zoo.admin.utils.ObjectMapper;

import java.util.Collections;
import java.util.List;

@Service
public class PlaceService {

    public List<Object> getAllPlaces() {

        ResponseEntity<PlaceDTO[]> response = new RestTemplate().exchange("http://localhost:8092/places", HttpMethod.GET, HttpEntity.EMPTY, PlaceDTO[].class);
        if (response.hasBody()) {
            PlaceDTO[] arrayPlaces = response.getBody();

            return ObjectMapper.arrayToList(arrayPlaces);
        }
        return Collections.emptyList();
    }

    public List<Object> getPlace(String name) {

        ResponseEntity<PlaceDTO[]> response = new RestTemplate().exchange("http://localhost:8092/places/" + name, HttpMethod.GET, HttpEntity.EMPTY, PlaceDTO[].class);
        if (response.hasBody()) {
            PlaceDTO[] arrayPlace = response.getBody();

            return ObjectMapper.arrayToList(arrayPlace);
        }
        return Collections.emptyList();


    }

    public PlaceDTO addPlace(PlaceDTO placeDTO) {

        HttpEntity httpEntity = new HttpEntity<>(placeDTO);

        ResponseEntity<PlaceDTO> response = new RestTemplate().exchange("http://localhost:8092/places", HttpMethod.POST, httpEntity, PlaceDTO.class);

        if (response.hasBody()) {
            return response.getBody();
        }
        return PlaceDTO.builder().build();

    }

    public void deletePlace(String name) {

        new RestTemplate().exchange("http://localhost:8092/places/" + name, HttpMethod.DELETE, HttpEntity.EMPTY, PlaceDTO.class);
    }
}
