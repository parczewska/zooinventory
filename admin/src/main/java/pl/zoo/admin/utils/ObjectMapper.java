package pl.zoo.admin.utils;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ObjectMapper {

    public static List<Object> arrayToList(Object[] objects) {

        if (objects != null) {
            return Arrays.asList(objects);
        }
        return Collections.emptyList();

    }
}
