create table zoo_animals(
id serial primary key,
species varchar(100) not null,
food varchar(50) not null,
number_of_animals int not null,
danger varchar(100) not null);