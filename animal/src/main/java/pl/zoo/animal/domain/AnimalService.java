package pl.zoo.animal.domain;

import org.springframework.stereotype.Service;
import pl.zoo.animal.model.Animal;
import pl.zoo.animal.model.AnimalDTO;
import pl.zoo.animal.model.AnimalMapping;

import java.util.List;
import java.util.Optional;

@Service
public class AnimalService {

    private AnimalRepository animalRepository;

    public AnimalService(AnimalRepository animalRepository) {

        this.animalRepository = animalRepository;
    }

    public List<AnimalDTO> findAll() {

        List<Animal> animals = animalRepository.findAll();
        return AnimalMapping.map(animals);
    }

    public List<AnimalDTO> findAllBySpecies(String species) {

        List<Animal> animals = animalRepository.findAllBySpecies(species);
        return AnimalMapping.map(animals);
    }

    public AnimalDTO add(AnimalDTO animalDTO) {

        Animal animal = new Animal();
        animal.setSpecies(animalDTO.getSpecies());
        animal.setFood(animalDTO.getFood());
        animal.setNumberOfAnimals(animalDTO.getNumberOfAnimals());
        animal.setDanger(animalDTO.getDanger());
        animalRepository.save(animal);
        return animalDTO;
    }

    public void delete(String species) {

        Optional<Animal> animal = animalRepository.findBySpecies(species);

        if (animal.isPresent()) {
            animalRepository.delete(animal.get());
        }
    }
}
