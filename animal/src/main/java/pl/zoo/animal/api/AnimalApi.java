package pl.zoo.animal.api;

import org.springframework.web.bind.annotation.*;
import pl.zoo.animal.domain.AnimalService;
import pl.zoo.animal.model.AnimalDTO;

import java.util.List;

@RestController
public class AnimalApi {

    private AnimalService animalService;

    public AnimalApi(AnimalService animalService) {
        this.animalService = animalService;
    }

    @RequestMapping(value = "/animals", method = RequestMethod.GET)
    public List<AnimalDTO> animals() {
        return animalService.findAll();
    }

    @RequestMapping(value = "/animals/{species}", method = RequestMethod.GET)
    public List<AnimalDTO> findBySpecies(@PathVariable String species) {
        return animalService.findAllBySpecies(species);
    }

    @RequestMapping(value = "/animals", method = RequestMethod.POST)
    public AnimalDTO add(@RequestBody AnimalDTO animalDTO) {
        return animalService.add(animalDTO);
    }

    @RequestMapping(value = "/animals/{species}", method = RequestMethod.DELETE)
    public void delete(@PathVariable String species) {
        animalService.delete(species);
    }
}
