package pl.zoo.animal.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import java.util.ArrayList;
import java.util.List;

@Builder
public class AnimalDTO {

    @JsonProperty
    private String species;
    @JsonProperty
    private String food;
    @JsonProperty
    private int numberOfAnimals;
    @JsonProperty
    @Enumerated(EnumType.STRING)
    private Danger danger;

    public String getSpecies() {
        return species;
    }

    public String getFood() {
        return food;
    }

    public int getNumberOfAnimals() {
        return numberOfAnimals;
    }

    public Danger getDanger() {
        return danger;
    }
}


