package pl.zoo.animal.model;

public enum Danger {

    LOW,
    MEDIUM,
    HIGH,
    VERY_HIGH;
}
