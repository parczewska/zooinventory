package pl.zoo.animal.model;

import java.util.ArrayList;
import java.util.List;

public class AnimalMapping {

    public static List<AnimalDTO> map(List<Animal> animals) {

        List<AnimalDTO> animalsDTO = new ArrayList<>();

        for (Animal animal : animals) {

            AnimalDTO animalDTO = AnimalDTO.builder().
                    species(animal.getSpecies()).
                    food(animal.getFood()).
                    numberOfAnimals(animal.getNumberOfAnimals()).
                    danger(animal.getDanger()).build();

            animalsDTO.add(animalDTO);
        }
        return animalsDTO;
    }
}
