package pl.zoo.animal.model;

import javax.persistence.*;

@Entity
@Table(name = "zoo_animals")
public class Animal {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "species")
    private String species;
    @Column(name = "food")
    private String food;
    @Column(name = "number_of_animals")
    private int numberOfAnimals;
    @Column(name = "danger")
    @Enumerated(EnumType.STRING)
    private Danger danger;

    public Animal() {
    }

    public long getId() {
        return id;
    }

    public String getSpecies() {
        return species;
    }

    public String getFood() {
        return food;
    }

    public int getNumberOfAnimals() {
        return numberOfAnimals;
    }

    public Danger getDanger() {
        return danger;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public void setFood(String food) {
        this.food = food;
    }

    public void setNumberOfAnimals(int numberOfAnimals) {
        this.numberOfAnimals = numberOfAnimals;
    }

    public void setDanger(Danger danger) {
        this.danger = danger;
    }
}
