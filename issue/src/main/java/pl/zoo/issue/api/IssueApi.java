package pl.zoo.issue.api;

import org.springframework.web.bind.annotation.*;
import pl.zoo.issue.domain.IssueService;
import pl.zoo.issue.model.IssueDTO;

import java.util.List;
import java.util.Optional;

@RestController
public class IssueApi {

    private IssueService issueService;

    public IssueApi(IssueService issueService) {
        this.issueService = issueService;
    }

    @RequestMapping(value = "/issues", method = RequestMethod.GET)
    public List<IssueDTO> issues() {
        return issueService.issues();
    }

    @RequestMapping(value = "/issues/{id}", method = RequestMethod.GET)
    public IssueDTO findById(@PathVariable long id) {
        return issueService.findById(id);
    }

    @RequestMapping(value = "/issues", method = RequestMethod.POST)
    public IssueDTO add(@RequestBody IssueDTO issueDTO) {
        return issueService.add(issueDTO);
    }

    @RequestMapping(value = "/issues/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable long id) {
        issueService.delete(id);
    }
}
