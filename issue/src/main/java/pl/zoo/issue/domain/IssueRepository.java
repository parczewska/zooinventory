package pl.zoo.issue.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.zoo.issue.model.Issue;

import java.util.List;
import java.util.Optional;

public interface IssueRepository extends JpaRepository<Issue, Long> {

    List<Issue> findByTitle(String title);

}
