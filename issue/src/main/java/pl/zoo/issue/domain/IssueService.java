package pl.zoo.issue.domain;

import org.springframework.stereotype.Service;
import pl.zoo.issue.model.Issue;
import pl.zoo.issue.model.IssueDTO;
import pl.zoo.issue.model.IssueMapping;

import java.util.List;
import java.util.Optional;

@Service
public class IssueService {

    private IssueRepository issueRepository;

    public IssueService(IssueRepository issueRepository) {
        this.issueRepository = issueRepository;
    }

    public List<IssueDTO> issues() {
        List<Issue> issues = issueRepository.findAll();
        return IssueMapping.map(issues);
    }

    public IssueDTO findById(long id) {
        Optional<Issue> issues = issueRepository.findById(id);
        return IssueMapping.map(issues);
    }

    public IssueDTO add(IssueDTO issueDTO) {

        Issue issue = new Issue();
        issue.setTitle(issueDTO.getTitle());
        issue.setDescription(issueDTO.getDescription());
        issue.setDate(issueDTO.getDate());
        issue.setHour(issueDTO.getHour());
        issueRepository.save(issue);

        return issueDTO;
    }

    public void delete(long id) {

        Optional<Issue> issue = issueRepository.findById(id);

        if (issue.isPresent()) {
            issueRepository.delete(issue.get());
        }
    }
}
