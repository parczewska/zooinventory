package pl.zoo.issue.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class IssueMapping {

    public static List<IssueDTO> map(List<Issue> issues) {

        List<IssueDTO> issuesDTO = new ArrayList<>();

        for (Issue issue : issues) {
            IssueDTO issueDTO = IssueDTO.builder().
                    title(issue.getTitle()).
                    description(issue.getDescription()).
                    date(issue.getDate()).
                    hour(issue.getHour()).build();

            issuesDTO.add(issueDTO);
        }
        return issuesDTO;
    }

    public static IssueDTO map(Optional<Issue> result) {

        if (result.isPresent()) {

            Issue issue = result.get();

            IssueDTO issueDTO = IssueDTO.builder().
                    title(issue.getTitle()).
                    description(issue.getDescription()).
                    date(issue.getDate()).
                    hour(issue.getHour()).build();
            return issueDTO;
        }
        return IssueDTO.builder().build();
    }
}
