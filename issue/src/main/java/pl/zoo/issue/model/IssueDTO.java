package pl.zoo.issue.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

import javax.persistence.Column;
import javax.persistence.Id;

@Builder
public class IssueDTO {

    @JsonProperty
    private String title;
    @JsonProperty
    private String description;
    @JsonProperty
    private String date;
    @JsonProperty
    private String hour;

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getDate() {
        return date;
    }

    public String getHour() {
        return hour;
    }
}
