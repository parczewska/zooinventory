create table zoo_issues(
id serial primary key,
title varchar(100) not null,
description varchar(250) not null,
date varchar(50) not null,
hour varchar (50) not null);