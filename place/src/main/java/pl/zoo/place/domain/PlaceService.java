package pl.zoo.place.domain;

import pl.zoo.place.model.Place;
import pl.zoo.place.model.PlaceDTO;
import pl.zoo.place.model.PlaceMapping;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PlaceService {

    private PlaceRepository placeRepository;

    public PlaceService(PlaceRepository placeRepository) {
        this.placeRepository = placeRepository;
    }

    public List<PlaceDTO> findAll() {

        List<Place> places = placeRepository.findAll();
        return PlaceMapping.map(places);
    }

    public PlaceDTO findByName(String name) {

        Optional<Place> places = placeRepository.findByName(name);
        return PlaceMapping.map(places);
    }

    public PlaceDTO add(PlaceDTO placeDTO) {

        Place place = new Place();
        place.setName(placeDTO.getName());
        place.setArea(placeDTO.getArea());
        placeRepository.save(place);

        return placeDTO;
    }

    public void delete(String name) {

        Optional<Place> place = placeRepository.findByName(name);

        if (place.isPresent()) {
            placeRepository.delete(place.get());
        }
    }
}
