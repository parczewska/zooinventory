package pl.zoo.place.api;

import org.springframework.web.bind.annotation.*;
import pl.zoo.place.model.Place;
import pl.zoo.place.model.PlaceDTO;
import pl.zoo.place.domain.PlaceService;

import java.util.List;

@RestController
public class PlaceApi {

    private PlaceService placeService;

    public PlaceApi(PlaceService placeService) {
        this.placeService = placeService;
    }

    @RequestMapping(value = "/places", method = RequestMethod.GET)
    public List<PlaceDTO> places() {
        return placeService.findAll();
    }

    @RequestMapping(value = "/places/{name}")
    public PlaceDTO findByName(@PathVariable String name) {
        return placeService.findByName(name);
    }

    @RequestMapping(value = "/places", method = RequestMethod.POST)
    public PlaceDTO add(@RequestBody PlaceDTO placeDTO) {
        return placeService.add(placeDTO);
    }

    @RequestMapping(value = "/places/{name}", method = RequestMethod.DELETE)
    public void delete(@PathVariable String name) {
        placeService.delete(name);
    }
}
