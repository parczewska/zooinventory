package pl.zoo.place.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

@Builder
public class PlaceDTO {

    @JsonProperty
    private String name;
    @JsonProperty
    private double area;

    public String getName() {
        return name;
    }

    public double getArea() {
        return area;
    }
}
