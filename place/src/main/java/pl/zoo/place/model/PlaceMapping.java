package pl.zoo.place.model;

import pl.zoo.place.model.Place;
import pl.zoo.place.model.PlaceDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PlaceMapping {

    public static List<PlaceDTO> map(List<Place> places) {

        List<PlaceDTO> placesDTO = new ArrayList<>();

        for (Place place : places) {
            PlaceDTO placeDTO = PlaceDTO.builder().
                    name(place.getName()).
                    area(place.getArea()).build();

            placesDTO.add(placeDTO);
        }
        return placesDTO;
    }

    public static PlaceDTO map(Optional<Place> result) {

        if (result.isPresent()) {

            Place place = result.get();

            PlaceDTO placeDTO = PlaceDTO.builder().
                    name(place.getName()).
                    area(place.getArea()).build();
            return placeDTO;
        }
        return PlaceDTO.builder().build();


    }
}
