create table zoo_places(
id serial primary key,
name varchar(100) not null,
area double precision not null);