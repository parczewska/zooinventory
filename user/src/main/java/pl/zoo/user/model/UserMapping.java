package pl.zoo.user.model;

import java.util.ArrayList;
import java.util.List;

public class UserMapping {

    public static List<UserDTO> map(List<User> users) {

        List<UserDTO> usersDTO = new ArrayList<>();

        for (User user : users) {

            UserDTO userDTO = UserDTO.builder().
                    name(user.getName()).
                    lastname(user.getLastname()).
                    profession(user.getProfession()).build();

            usersDTO.add(userDTO);
        }
        return usersDTO;
    }
}
