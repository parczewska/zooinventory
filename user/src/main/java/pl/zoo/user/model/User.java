package pl.zoo.user.model;

import org.springframework.boot.autoconfigure.security.saml2.Saml2RelyingPartyAutoConfiguration;

import javax.persistence.*;

@Entity
@Table(name = "zoo_users")
public class User {

    @Id
    private String login;
    @Column(name = "name")
    private String name;
    @Column(name = "lastname")
    private String lastname;
    @Column(name = "profession")
    private String profession;
    @Column(name = "password")
    private String password;

    public String getName() {
        return name;
    }

    public String getLastname() {
        return lastname;
    }

    public String getProfession() {
        return profession;
    }

    public String getPassword() {
        return password;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
