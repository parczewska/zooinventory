package pl.zoo.user.api;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.zoo.user.domain.UserService;
import pl.zoo.user.model.UserDTO;

import java.util.List;

@RestController
public class UserApi {

    private UserService userService;

    public UserApi(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public List<UserDTO> users() {
        return userService.findAll();
    }

    @RequestMapping(value = "/users/{lastname}", method = RequestMethod.GET)
    public List<UserDTO> find(@PathVariable String lastname) {
        return userService.findAllByLastname(lastname);
    }

    @RequestMapping(value = "/users", method = RequestMethod.POST)
    public UserDTO add(@RequestBody UserDTO userDTO) {
        return userService.add(userDTO);
    }

    @RequestMapping(value = "/users/{login}", method = RequestMethod.DELETE)
    public void delete(@PathVariable String login) {
        userService.delete(login);
    }

    @RequestMapping(value = "/users/login", method = RequestMethod.GET)
    public ResponseEntity<Void> findUser(@RequestParam(name = "login") String login, @RequestParam(name = "password") String password) {
        boolean success = userService.findAdmin(login, password);
        if (success) {
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN.value()).build();
    }

}
