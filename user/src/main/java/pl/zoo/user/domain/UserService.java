package pl.zoo.user.domain;

import org.springframework.stereotype.Service;
import pl.zoo.user.model.User;
import pl.zoo.user.model.UserDTO;
import pl.zoo.user.model.UserMapping;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    private UserRepository userRepository;

    public UserService(UserRepository userRepository) {

        this.userRepository = userRepository;
    }

    public List<UserDTO> findAll() {

        List<User> users = userRepository.findAll();
        return UserMapping.map(users);
    }

    public List<UserDTO> findAllByLastname(String lastname) {

        List<User> users = userRepository.findAllByLastname(lastname);
        return UserMapping.map(users);
    }

    public UserDTO add(UserDTO userDTO) {

        Optional<User> loginExist = userRepository.findByLogin(userDTO.getLogin());

        if (loginExist.isEmpty()) {

            User user = new User();
            user.setLogin(userDTO.getLogin());
            user.setName(userDTO.getName());
            user.setLastname(userDTO.getLastname());
            user.setProfession(userDTO.getProfession());
            user.setPassword(userDTO.getPassword());
            userRepository.save(user);
            return userDTO;
        }
        return UserDTO.builder().build();
    }

    public void delete(String login) {

        Optional<User> user = userRepository.findByLogin(login);

        if (user.isPresent()) {
            userRepository.delete(user.get());
        }
    }

    public boolean findAdmin(String login, String password) {

        Optional<User> result = userRepository.findByLogin(login);

        if (result.isPresent()) {

            User user = result.get();

            return user.getProfession().equals("admin") && user.getPassword().equals(password);
        }
        return false;
    }
}