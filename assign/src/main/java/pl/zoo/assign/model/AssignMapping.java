package pl.zoo.assign.model;

import java.util.ArrayList;
import java.util.List;

public class AssignMapping {

    public static List<AssignDTO> map(List<Assign> assigns) {

        List<AssignDTO> assignsDTO = new ArrayList<>();

        for (Assign assign : assigns) {
            AssignDTO assignDTO = AssignDTO.builder().
                    id(assign.getId()).build();

            assignsDTO.add(assignDTO);
        }
        return assignsDTO;
    }
}
