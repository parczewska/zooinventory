package pl.zoo.assign.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

@Builder
public class AssignDTO {

    @JsonProperty
    private long id;
}
