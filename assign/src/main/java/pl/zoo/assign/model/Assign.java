package pl.zoo.assign.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "zoo_assign")
public class Assign {

    @Id
    private long id;

    public long getId(){
        return id;
    }
}
