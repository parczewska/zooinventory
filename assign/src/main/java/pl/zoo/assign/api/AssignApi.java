package pl.zoo.assign.api;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import pl.zoo.assign.domain.AssignService;
import pl.zoo.assign.model.AssignDTO;

import java.util.List;

@RestController
public class AssignApi {

    private AssignService assignService;

    public AssignApi(AssignService assignService) {
        this.assignService = assignService;
    }

    @RequestMapping(value = "assigns", method = RequestMethod.GET)
    public List<AssignDTO> assigns() {
        return assignService.findAll();
    }
}
