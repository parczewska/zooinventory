package pl.zoo.assign.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.zoo.assign.model.Assign;

public interface AssignRepository extends JpaRepository<Assign, Long> {
}
