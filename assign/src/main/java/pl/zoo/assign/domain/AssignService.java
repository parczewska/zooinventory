package pl.zoo.assign.domain;

import org.springframework.stereotype.Service;
import pl.zoo.assign.model.Assign;
import pl.zoo.assign.model.AssignDTO;
import pl.zoo.assign.model.AssignMapping;

import java.util.List;

@Service
public class AssignService {

    AssignRepository assignRepository;

    public AssignService(AssignRepository assignRepository) {
        this.assignRepository = assignRepository;
    }

    public List<AssignDTO> findAll() {
        List<Assign> assigns = assignRepository.findAll();
        return AssignMapping.map(assigns);
    }


}
